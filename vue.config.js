module.exports = {
  devServer: {
    https: true,
    host: process.env.VUE_BASE_URL,
    port: 8081
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  }
}

import GamesList from '@/views/Private/Pages/Games/List.vue'
import ArmiesList from '@/views/Private/Pages/Armies/List.vue'
import ArmiesEdit from '@/views/Private/Pages/Armies/Edit.vue'
import ArmiesAdd from '@/views/Private/Pages/Armies/Add.vue'
import LogList from '@/views/Private/Pages/Logs/List.vue'

export default [
  {
    path: '/',
    name: 'games',
    redirect: '/games'
  },
  {
    path: '/',
    name: 'games/:game/log',
    component: LogList
  },
  {
    path: '/armies/add/new',
    name: 'army-new',
    component: ArmiesAdd
  },
  {
    path: '/armies/:id/edit',
    name: 'army-edit',
    component: ArmiesEdit
  },
  {
    path: '/armies',
    name: 'army-list',
    component: ArmiesList
  },
  {
    path: '/games',
    name: 'game-list',
    component: GamesList
  },
  {
    path: '/logs/:id',
    name: 'logs-game',
    component: LogList
  }
]

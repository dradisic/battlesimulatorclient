import Vue from 'vue'
import Router from 'vue-router'
import { sync } from 'vuex-router-sync'
import store from '@/store'

import PublicLayout from '@/views/Public/Layout'
import PrivateLayout from '@/views/Private/Layout'

import PrivateRoutes from '@/routes/privateRoutes'
import PublicRoutes from '@/routes/publicRoutes'

import Error404 from '@/views/Error404.vue'

Vue.use(Router)
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: PublicLayout,
      children: PublicRoutes
    },
    {
      path: '/',
      component: PrivateLayout,
      children: PrivateRoutes,
      meta: { requiresAuth: true }
    },
    {
      path: '*',
      name: 'error404',
      component: Error404
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.isAuthenticated) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    }
  }
  next()
})

sync(store, router)
export default router

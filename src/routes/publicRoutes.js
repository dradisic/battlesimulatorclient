import Login from '@/views/Public/Pages/Login.vue'
import Registration from '@/views/Public/Pages/Registration.vue'

export default [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/registration',
    component: Registration
  }
]

import Vue from 'vue'
import App from '@/App.vue'
import ElementUI from 'element-ui'
import 'element-theme-chalk'
import locale from 'element-ui/lib/locale/lang/en'
import router from '@/routes/index'
import store from '@/store'
import API from '@/api'

Vue.use(ElementUI, { locale })

Vue.prototype.$api = API
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')

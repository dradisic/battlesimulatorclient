import axios from 'axios'
import store from '@/store'
import qs from 'qs'

const api = axios.create({
  baseURL: process.env.VUE_APP_API + '/' + process.env.VUE_APP_API_VERSION,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  },
  paramsSerializer: function (params) {
    return qs.stringify(params, { arrayFormat: 'indicies' })
  }
})

api.interceptors.request.use(async (config) => {
  if (isAccessTokenExpired()) {
    if (getRefreshToken()) {
      await loadAccessToken().then(response => {
        store.dispatch('setAuth', response.data)
      })
        .catch(() => {
          store.dispatch('setLogout').then(() => {
            window.location.href = '/login'
          })
        })
    }
  }
  let token = getAccessToken()
  if (token) {
    config.headers.Authorization = 'Bearer ' + token
  }

  return config
})
//
api.interceptors.response.use(async (response) => {
  return response
}, async (error) => {
  if (Object.getPrototypeOf(error) === axios.Cancel.prototype) {
    return new Promise(() => {
    })
  }
  if (error.response.data.error.code === 1002) {
    if (getRefreshToken()) {
      await loadAccessToken().then(response => {
        store.dispatch('setAuth', response.data)
      })
        .catch(() => {
          store.dispatch('setLogout').then(() => {
            window.location.href = '/login'
          })
        })
    }
  }

  return Promise.reject(error)
})

function isAccessTokenExpired () {
  return (Object.keys(store.state.auth).length === 0 && store.state.auth.constructor === Object) ? false : Math.round(Date.now() / 1000) > store.state.auth.expires_at
}

function getRefreshToken () {
  return (Object.keys(store.state.auth).length === 0 && store.state.auth.constructor === Object) ? false : store.state.auth.refreshToken
}

function getAccessToken () {
  return store.state.auth.accessToken
}

function loadAccessToken () {
  return axios.post(process.env.VUE_APP_API + '/login/refresh', {
    'refreshToken': getRefreshToken()
  })
}

export default api
